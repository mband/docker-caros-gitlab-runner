#!/bin/bash

########################################################################
#### TODO:
#### - Add support for specifying what revision to checkout/setup
########################################################################

set -o nounset
#set -o errexit

repo_folder="$(pwd)/robwork_trunk"

function do_job() {
    if [ ${#} -ne 1 ]; then
	echo "do_job needs 1 argument"
	exit 1
    fi

    case "${1}" in
	"setup")
	    if [ -d "${repo_folder}" ]; then
		do_job "clear"
	    fi
	    /usr/bin/svn checkout --revision 5820 --non-interactive --username Guest https://svnsrv.sdu.dk/svn/RobWork/trunk "${repo_folder}"
	    ;;
	"clear")
	    if [ ! -d "${repo_folder}" ]; then
		echo "${repo_folder} doesn't exist!"
		exit 1
	    fi
	    /bin/rm -fr "${repo_folder}"
	    ;;
	*)
	    echo "Unknown command: ${1}"
	    exit 1
    esac
}

if [ ${#} -ne 1 ]; then
    echo "Usage ${0} command"
    exit 1
fi

do_job "${1}"

exit 0
