FROM ubuntu:14.04

MAINTAINER mband

RUN sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key 0xB01FA116

RUN apt-get update && apt-get install -y \
    gcc \
    g++ \
    cmake \
    ros-indigo-ros-base \
    ros-indigo-roslint \
    python-catkin-lint

RUN rosdep init && rosdep update

RUN apt-get install -y curl
RUN curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | bash

RUN apt-get install -y gitlab-ci-multi-runner

########################################################################
# RobWork
########################################################################
RUN apt-get install -y \
    gcc \
    g++ \
    cmake \
    libatlas-base-dev \
    libxerces-c3.1 \
    libxerces-c-dev \
    libboost-dev \
    libboost-date-time-dev \
    libboost-filesystem-dev \
    libboost-program-options-dev \
    libboost-regex-dev \
    libboost-serialization-dev \
    libboost-system-dev \
    libboost-test-dev \
    libboost-thread-dev \
    swig \
    qt4-dev-tools \
    libqt4-dev \
    qt4-designer \
    libode-dev \
    libode1

# Extras for SWIG
RUN apt-get install -y \
    liblua5.1-0-dev \
    liblua5.2-dev \
    python-dev \
    default-jdk

RUN apt-get install -y \
    subversion

# Setup svn server certificate trusting
RUN mkdir -p /root/.subversion/auth/svn.ssl.server
COPY dot_subversion_auth_svn.ssl.server_96b5b1f8c4ccc6bf8ff70dbdc5041b33 /root/.subversion/auth/svn.ssl.server/96b5b1f8c4ccc6bf8ff70dbdc5041b33
# Setup svn user account
RUN mkdir -p /root/.subversion/auth/svn.simple
COPY dot_subversion_auth_svn.simple_e22045cb2f9772d79a3d577a29839b93 /root/.subversion/auth/svn.simple/e22045cb2f9772d79a3d577a29839b93

RUN mkdir /robwork
COPY robwork-setup-controller.sh /robwork/robwork-setup-controller.sh
RUN chmod 700 /robwork/robwork-setup-controller.sh
COPY robwork-build-controller.sh /robwork/robwork-build-controller.sh
RUN chmod 700 /robwork/robwork-build-controller.sh
########################################################################

########################################################################
#### Configure gitlab ci runner
########################################################################
RUN gitlab-ci-multi-runner register --non-interactive --registration-token 4b094a8c499029d1cd8d28efd05548 --executor shell --url https://gitlab.com/ci --name "caros-gitlab-runner"
########################################################################

COPY entrypoint.sh /entrypoint.sh
RUN chmod 700 /entrypoint.sh


ENTRYPOINT ["/entrypoint.sh"]
