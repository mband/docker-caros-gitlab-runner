#!/bin/bash

set -o nounset
#set -o errexit

repo_folder="$(pwd)/robwork_trunk"
projects="RobWork RobWorkStudio RobWorkSim RobWorkHardware"
build_prefix="build-"
job_count="$(nproc)"

if [ ! -d "${repo_folder}" ]; then
    echo "repo_folder: ${repo_folder} doesn't exist!"
    exit 123
fi

function do_job() {
    if [ ${#} -ne 2 ]; then
	echo "do_job needs two arguments"
	exit 1
    fi

    project_dir="${repo_folder}/${2}"
    if [ ! -d "${project_dir}" ]; then
	echo "${project_dir} doesn't exist!"
	exit 1
    fi

    build_dir="${build_prefix}${2}"
    if [ ! -d "${build_dir}" ]; then
	mkdir "${build_dir}"
    fi

    if [ -f "dot_ycm_extra_conf.py" ]; then
	sed -e "s#\(compilation_database_folder = \)'\(.\+\)'#\1'$(pwd)/${build_dir}'#g" "dot_ycm_extra_conf.py" > "${project_dir}/.ycm_extra_conf.py"
    else
	echo "Didn't find any dot_ycm_extra_conf.py file!"
    fi

    case "${1}" in
	"clean")
	    (cd "${build_dir}"; make clean)
	    ;;
	"clear")
	    do_job "clean" "${2}"
	    if [ -d "${build_dir}" ]; then
		rm -r "${build_dir}"
	    else
		# This should never happen unless 'mkdir "${build_dir}"' above failed
		echo "${build_dir} doesn't exist!"
		exit 1
	    fi
	    ;;
	"cmake")
	    rw_cxx_flags_extra="-std=c++11"
	    # -DRW_CXX_FLAGS_EXTRA will only be used by RobWork (the other projects will complain that the user defined variable is not being used)
	    (cd "${build_dir}"; cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DRW_CXX_FLAGS_EXTRA="${rw_cxx_flags_extra}" "${project_dir}")
	    ;;
	"make")
	    (cd "${build_dir}"; make -j ${job_count})
	    do_job "env" "${2}"
	    ;;
	"build")
	    do_job "clean" "${2}"
	    do_job "cmake" "${2}"
	    do_job "make" "${2}"
	    do_job "env" "${2}"
	    ;;
	"env"|"environment")
	    cat > robwork-environment-setup.bash <<EOF	    
export RW_ROOT="${repo_folder}/RobWork"
export RWS_ROOT="${repo_folder}/RobWorkStudio"
export RWHW_ROOT="${repo_folder}/RobWorkHardware"
EOF
	    ;;
	*)
	    echo "Unknown command: ${1}"
	    exit 1
    esac
}

if [ ${#} -ne 1 -a ${#} -ne 2 ]; then
    echo "Usage ${0} command [\"project1[ ...]\"]"
    exit 1
fi
if [ ${#} -eq 2 ]; then
    projects="${2}"
fi
for p in ${projects}; do
    do_job "${1}" "${p}"
done

exit 0
