#!/bin/bash

set -o nounset
set -o errexit

#exec /bin/bash
exec /usr/bin/gitlab-ci-multi-runner run
